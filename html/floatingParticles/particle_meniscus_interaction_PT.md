autor: Julien Chopin
título: Dinâmica de partículas flutuantes próximas de um menisco.
descrição: Breve tutorial para ilustrar com um vídeo a interação atrativa e repulsiva entre um menisco e partículas flutuantes. 

# Dinâmica das partículas flutuantes perto de um menisco

## Materiais

- Grãos Flutuantes (Poliestireno,pérola de plástico, lentilhas, sementes, quinoa,... )
- Copo d'água
- Smartphone
- Palito de dente
- Tripé
- Jarro com água
- Livros, caixas ... para ajustar a altura

**Opcional**

- Iluminação adicional (luz do smartphone, tela do computador como luz de fundo)
- Seringa

## Objetivo

Explore a dinâmica de partículas flutuantes perto de um menisco qualquer

## Setup experimental 

![setup1](Media/setup1.jpeg)

![setup2](Media/setup2.jpeg)

## Algum background

- [ ] Belas fotos / vídeos de um menisco com curvatura oposta para ilustrar o parágrafo seguinte.

A água dentro do copo tem uma superfície plana horizontal (princípio de Arquimedes), exceto perto da região do menisco onde o líquido está em contato com o copo.

O vidro é um material hidrofílico (quando limpo), o que significa que a água tende a se espalhar quando em contato com ela. Assim, a água na região do menisco sobe no vidro. A altura do menisco resulta de uma competição entre o efeito de superfície (aqui hidrofilicidade) e a gravidade. Normalmente, para água no copo, é da ordem de alguns milímetros.

A curvatura do menisco pode ser invertida despejando água acima da borda do copo.

> Em geral, qualquer superfície sólida deformaria uma interface de líquido perto de sua superfície. Podem ser paredes de um recipiente ou partículas flutuando em um líquido.

Agora, uma partícula flutuante deforma localmente a interface do líquido seguindo o mesmo mecanismo de antes.

Essas deformações de interface carregam um custo energético proporcional à mudança na área de superfície. Agora, uma partícula flutuante pode mover-se para perto ou para longe do menisco para reduzir a área de interface resultante da deformação induzida da borda e da partícula.



## Instrução

Materiais diferentes exercem influências diferentes. Enquanto uns exercem uma força atrativa (materiais hidrofílicos), outros exercem uma força repulsiva (hidrofóbicos). Explore diferentes materiais e avalie a força exercida através da curvatura do menisco em contato com as partículas.

### Experimentando

Separe um copo com água e partículas de diferentes materiais. Em seguida, ponha partículas na sua superfície da água e veja como se dá o comportamento da curvatura menisco e do movimento da partícula.

#### Dicas:

- Explore como a curvatura do menisco é influenciada pela altura da altura da coluna d’água.

- Opções de partículas: bolinhas de cereal, bolinhas de isopor, pétalas de flores, bolinha de papel, pena, etc.

- Varie o material do copo (aço inox, plástico, etc) e também o líquido (óleo, iogurte).

#### Perguntas:

- O que acontece quando dois materiais hidrofílicos estão próximos? E os hidrofóbicos?

- O que acontece quando uma partícula hidrofílica e uma hidrofóbica se encontram?

**Curiosidade extra:** Como deve se comportar o menisco na interface entre dois líquidos diferentes? 



#### Uma análise mais avançada:

Avalie a dinâmica do movimento das partículas!
 O CVmob é uma ferramenta que nos permite, em poucas palavras, “seguir” os passos de um ponto marcado num vídeo. Aplicando ao experimento dar partículas flutuantes, o CVmob vai permitir a avaliação da velocidade das partículas flutuantes. 

Aqui você acha um rápido tutorial com vídeos de experimentos virtuais para aprender a mexer com o CVmob: http://www.fis.ufba.br/sites/fis.ufba.br/files/cvmob.pdf. 

### Em Resumo

1. Coloque as partículas em um copo cheio de água. Diminua e aumente o nível de água e observe se a interação com o menisco é atrativa ou repulsiva.
2. Altere o tipo de partícula e observe se há alteração do sinal da interação.
3. Explore a interação entre duas partículas semelhantes. Junte duas partículas cuja interação com o menisco seja oposta.
4. Faça um filme para as várias situações mencionadas acima e analise a dinâmica com CVmobi.
5. Você também pode trocar o líquido, usando óleo em vez de água por exemplo. Preste atenção na dinâmica, é mais rápido ou mais lento? Em vez de um copo cheio com o líquido, você pode ver se algo muda quando apenas uma fina camada de líquido é considerada. Talvez apenas a dinâmica mude.

## Análise das Partículas Flutuantes

![aguaNaMoeda](Media/aguaNaMoeda-1500.jpg)

A água pode elevar-se sobre uma superfície sem transbordar. Para tirar a imagem ilustrativa acima utilizei uma moeda de 10 centavos e uma seringa com água. O fluido foi depositado aos poucos e no processo diversas fotos foram tiradas. A imagem selecionada representa uma das últimas configurações de equilíbrio possíveis. Com a tentativa de depositar mais gotas essa poça se rompe e transborda.



### Observando as Interações de uma Partícula Flutuante

<iframe width="560" height="315" src="https://www.youtube.com/embed/W3y5M7TYp7U" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Neste vídeo vemos como a partícula de poliestireno, que inicialmente é atraída em direção à borda do copo, passa a ser atraída para o centro quando o copo é preenchido até acima da borda. Essa partícula sofre a ação de forças que a conduzem para o ponto mais alto do líquido. Um comportamento similar pode ser visto com pétalas.

<iframe width="560" height="315" src="https://www.youtube.com/embed/OOqty2Mn8uY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Conforme pôde ser visto no vídeo anterior, partículas similares tendem a se agregar. A atração entre duas partículas de poliestireno está mais explicitada no vídeo a seguir. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/3MGrcXo9fp8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Utilizei duas agulhas e um imã no último vídeo. As agulhas também se atraem, geralmente suas pontas se unem, como no vídeo. ou elas ficam grudadas estando paralelas entre si. O imã só está sendo utilizado para mover as agulhas sem um contato direto com as mesmas, seu uso é dispensável e pode acabar deixando as agulhas magnetizadas.

<iframe width="560" height="315" src="https://www.youtube.com/embed/KHB7k5Tjx1I" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Analisando o Movimento da Partícula numa Poça

Para a captura das imagens da partícula flutuante, utilizei uma mesa de vidro e posicionei o celular sobre uma cadeira. Como partícula utilizei uma bola de poliestireno.

Uma régua (esquadro no meu caso) sobre a mesa possibilita que depois associemos o número de pixels da imagem a uma medida em centímetros.

Com o aplicativo [Open Camera]("https://opencamera.org.uk/") foi mais fácil manter o objeto de interesse focado durante a filmagem.

Podemos simplesmente derramar um pouco d'água na mesa para forma uma poça. Empurrando a partícula sobre o tampo de vidro em direção a região molhada, vemos que quando a partícula se encontra com a água, surge uma força sobre a mesma que a conduz para pontos mais elevados da poça. Após repetir o procedimento algumas vezes a partícula deixou de ser atraída para o topo, em vez disso apenas espalhava mais a água da poça que "vazava" ao entrar em contato com a bola de poliestireno. Uma nova partícula, por outro lado, o fazia.

Vale ressaltar que nesse procedimento a poça acaba assumindo um formato bem assimétrico.

<iframe width="560" height="315" src="https://www.youtube.com/embed/YQGxWPm378I" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

O vídeo da partícula na poça, que aqui será analisado, foi carregado no CVmob, onde podemos:

- Clicar em dois  pontos da régua (no vídeo), ir em **Calibração/Iniciar Calibração**  e informar no aplicativo a distância entre esses pontos em centímetros.
- Clicar na Partícula e então no botão **Reproduzir**.
- Para que possamos usar no Python os dados aqui obtidos vamos em **Arquivo/Exportar/Trajetória**. Optei por salvar o arquivo no formato txt escolhendo o nome "Dados.txt".



Abrindo o arquivo txt vemos que os dados estão separados por um TAB. Se quiser trocar por ";" podemos utilizar o seguinte script:

```python
text = open("dados.txt", 'r')
new_text = open("dados2.txt", 'w')
new_text.write(text.read().replace('\t',';'))
text.close()
new_text.close()
```

Onde **dados.txt** é o nome do arquivo original e **dados2.txt** é o nome de um novo arquivo que será criado com a modificação desejada.

Para trocar ";" por "," podemos simplesmente, no próprio txt, utilizar o comando **Ctrl+H**.

Esse recurso não foi utilizado nesta analise. Mas pode ser útil para quem queira utilizar os dados no Excel, por exemplo.

Note que embora tenhamos calibrado o CVmob para medidas em centímetros os dados exportados estão no SI. 



Para construir os gráficos no Python, escrevi o script que segue:

```python
import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize

def fit_curve(time, Data):
    guess = [1, 0, 1]
    params, params_covariance = optimize.curve_fit(lambda t,A,B,k: A*np.e**(-k*t)+ B, time, Data, guess) #f(t) = Ae^(-kt) + B
    #params, params_covariance = optimize.curve_fit(lambda t,A,B,k: A/(np.e**(k*t)+np.e**(-k*t))+ B, time, Data, guess) #f(t) = A/[e^(kt)-e^(-kt)] + B
    print('A = %f\tB = %f\tk = %f'%(params[0],params[1],params[2]))
    plt.plot(time, params[0]*np.e**(-params[2]*time)+ params[1], label = 'Fitted Curve')

def traj(x,y, xlabel, ylabel, Title, proportional = False, fit = True):
    plt.scatter(x,y, marker='*', color = 'r', s=6, label = 'Experimental Data')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    if proportional:
        if abs(max(x)-min(x))> abs(max(y)-min(y)):
            interval = abs(max(x)-min(x))
        else:
            interval = abs(max(y)-min(y))

        plt.xlim(min(x),min(x)+interval)
        plt.ylim(min(y),min(y)+interval)                               #Proportion between axes
        plt.gca().set_aspect('equal', adjustable='box')

    if fit:
        print(Title)
        fit_curve(x, y)
        
    plt.title(Title)
    plt.grid(True)
    plt.legend()
    #plt.savefig(Title + '.png')
    plt.show()

    
def main():    
    #Reading data from txt file with numpy.
    time, vel, vel_x, vel_y, ac, ac_x, ac_y, work, x, y  = np.loadtxt('DadosPoça.txt', delimiter = '\t', skiprows=1, unpack = True, )

    #Coverting to cm
    for i in [vel, vel_x, vel_y, ac, ac_x, ac_y, x, y]:
        i*=100
    
    #Ploting Trajectory.   
    traj(x, y, "x(cm)", "y(cm)", "Trajectory", proportional = True, fit = False)
    traj(time,x, "t (s)", "x(cm)", 'x vs t')
    traj(time,y, "t (s)", "y(cm)", 'y vs t')
    traj(time,vel, "t (s)", "vel(cm/s)", 'Velocity vs t')
    traj(time,ac, "t (s)", "ac(cm/s²)", 'Acceleration vs t')
    traj(time,vel_x, "t (s)", "vel_x(cm/s)", 'Vx vs t')
    traj(time,vel_y, "t (s)", "vel_y(cm/s)", 'Vy vs t')   
          

main()
```

Primeiro temos o gráfico da trajetória descrita pela partícula. Note que as medidas dos eixos são proporcionais, o lado de cada quadrado da grade mede $0.2cm$.

Enquanto as posições $x$ variam de $15.1cm-14.6cm = 0.5cm$, as posições $y$  tem a variação $5.8cm-4.7cm = 1.1cm$, que é aproximadamente o dobro.

![Trajectory](Media/Graphs/Poliestireno/Trajectory.png)

Podemos ver também somente as projeções da posição nos eixos em função do tempo. Naturalmente, por razões de simetria, as curvas são similares, só estando invertidas com relação a outra. Isso ocorre somente por conta da posição em que colocamos a partícula e da maneira com a qual os eixos foram definidos no CVmob. Enquanto os valores de $x$ crescem com o tempo, os valores de $y$ decrescem.

<img src="Media/Graphs/Poliestireno/XvsT.png" alt="XvsT" style="zoom:70%;" /> <img src="Media/Graphs/Poliestireno/YvsT.png" alt="YvsT" style="zoom:70%;" />



O gráfico da velocidade, em módulo, como função do tempo é parecido não só com os gráficos das projeções da velocidade $`V_x`$ e $`V_y`$, como também se parece com o das projeções $`x`$ e $`y`$.  Todos estes gráficos parecem indicar uma solução do tipo $`f(t) =Ae^{-kt} + B`$.



<img src="Media/Graphs/Poliestireno/VelvsT.png" alt="VelvsT" style="zoom:70%;" />

Supondo que a solução é de fato deste tipo encontramos as constantes $`A`$, $`B`$ e $`k`$. 

As curvas azuis são os $`f(t)`$ correspondentes a cada gráfico.



|       |  x vs t   |  y vs t  | Vel vs t |
| :---: | :-------: | :------: | :------: |
| **A** | -0.928194 | 1.210012 | 1.557184 |
| **B** | 15.087328 | 4.604424 | 0.067560 |
| **k** | 1.305200  | 0.432653 | 1.174720 |

### Interação de Partículas com Percevejos

Utilizei também uma tampa plástica transparente, para não colocar água diretamente sobre a mesa. Com um pouco mais de água a curvatura do líquido é reduzida. No entanto podemos colocar algum objeto, um que não fique submerso na poça rasa, para que este cause uma deformação na superfície do líquido. Para este fim utilizei percevejos.

![percevejosDeformação](Media/percevejosDeformação-700.jpg)

#### Interação com Um Percevejo

Para analisar a dinâmica de uma partícula com um único percevejo utilizei uma conta de plástico e uma bola de poliestireno.

Como eu não tinha uma mesa de vidro à minha disposição no momento desta gravação, utilizei uma peça de vidro e filmei por cima.

<img src="Media/setup_percevejo-1200.jpg" alt="setup_percevejo-1200" style="zoom: 50%;" />

As imagens capturadas e analisadas se encontram no vídeo seguinte.

<iframe width="560" height="315" src="https://www.youtube.com/embed/l6dzjAc1N_s" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

A diferença de nível d'água pode originar deformações diferentes na vizinhança do percevejo. Por essa razão analiso a partícula para duas configurações de nível d'água.

Para deixar o fundo da tampa, que tem uma base de $`13.2cm`$ de diâmetro, completamente preenchido por água, precisei depositar $`50ml`$.  Ao depositar mais  $`25ml`$  a água preenche o espaço aberto pelo percevejo e a deformação se desfaz com a submersão do objeto.

<img src="Media/percevejo_deforação.jpg" alt="percevejo_deforação" style="zoom: 33%;" />

Vou fazer a análise na ordem na qual as filmagens foram dispostas no vídeo.

Em cada trecho, a origem foi posta aproximadamente na ponta do percevejo e a escala definida a partir das marcações no esquadro que pareciam mais nítidas e distantes entre si.

Ao exportar os dados diretamente pelo CVMob notei que os dados no arquivo criado não estavam utilizando o origem que impus. Os dados na tabela exibida no aplicativo, por outro lado, estavam. Antes de exportar a trajetória devemos lembrar de clicar em atualizar.

Podemos copiar a tabela diretamente do CVMob e colar numa planilha Excel. A primeira alternativa é ir em Arquivo/Exportar e então exportar como txt.

Para ler diretamente um arquivo excel pelo python podemos utilizar os comandos a seguir.

```python
from pandas import read_excel

frame, time, vel, ac, x, y, vel_x, vel_y ,ac_x, ac_y = np.hsplit(np.array(read_excel('Dados/conta_borda.csv', engine='openpyxl')),10)
```

Precisei instalar um módulo externo com `pip install openpyxl`.

##### Conta Atraída pela Borda

Primeiro temos o gráfico que exibe a trajetória da partícula que sobe em direção à borda.

<img src="Media/Graphs/conta_borda/traj.png" alt="traj" style="zoom:80%;" />

As curvas $`x=x(t)`$ e $`y=y(t)`$ foram modeladas com uma expressão polinomial e com uma combinação de exponencial com trigonométrica. Observamos novamente um comportamento oscilatório em torno da curva ajustada.

<img src="Media/Graphs/conta_borda/XvsT.png" alt="XvsT" style="zoom:70%;" /><img src="Media/Graphs/conta_borda/YvsT.png" alt="YvsT" style="zoom:70%;" />



##### Conta Atraída pelo Percevejo

As trajetórias descritas pela conta nas duas situações são postas juntas aqui para facilitar a comparação.

<img src="Media/Graphs/conta_percevejo/traj.png" alt="traj" style="zoom:74%;" /><img src="Media/Graphs/conta_percevejo/traj2.png" alt="traj" style="zoom:74%;" />

E o mesmo é feito para os demais gráficos.

<img src="Media/Graphs/conta_percevejo/XvsT.png" alt="XvsT" style="zoom:74%;" /><img src="Media/Graphs/conta_percevejo/XvsT2.png" alt="XvsT" style="zoom:74%;" />

<img src="Media/Graphs/conta_percevejo/XvsT.png" alt="YvsT" style="zoom:74%;" /><img src="Media/Graphs/conta_percevejo/YvsT2.png" alt="XvsT" style="zoom:74%;" />

##### Poliestireno Repelido pelo Pelo Percevejo

Fazemos o mesmo agora para a partícula de poliestireno.

<img src="Media/Graphs/poli_percevejo/traj.png" alt="traj" style="zoom:74%;" /><img src="Media/Graphs/poli_percevejo/traj2.png" alt="traj2" style="zoom:74%;" />



<img src="Media\Graphs\poli_percevejo\XvsT.png" alt="traj" style="zoom:74%;" /><img src="Media\Graphs\poli_percevejo\XvsT2.png" alt="traj2" style="zoom:74%;" />

<img src="Media\Graphs\poli_percevejo\YvsT.png" alt="traj" style="zoom:74%;" /><img src="Media\Graphs\poli_percevejo\YvsT2.png" alt="traj2" style="zoom:74%;" />



#### Interação com Três Percevejos

No [vídeo da partícula entre 3 percevejos](https://www.youtube.com/watch?v=PTwCRExh6EM) vemos que a bola de poliestireno é repelida pelos percevejos, mas é atraída por um ponto no líquido entre eles. Este é também um ponto de equilíbrio.

Ao final deste mesmo vídeo, também mostro que um pedaço de amarrilho, ou arame plástico, é atraído em direção aos percevejos.

Utilizando o CVmob e o mesmo script Python de antes gerei novos gráficos.

<img src="Media\Graphs\Amarrilho\Amarrilho.png" alt="YvsTAmarrilho" style="zoom:80%;" />

No gráfico acima vemos uma série de pontos à direita que representam momentos nos quais a partícula já se encontra parada.

Para removê-los com o Python apresento  duas opções. Podemos utilizar **max_rows** para limitar a quantidade de linhas lidas. 

```python
#len(time) ----> 58
np.loadtxt('Dados.txt', delimiter = '\t', skiprows=1, max_rows = 53-7, unpack = True, )
```

Outra alternativa é realizar um **slice** para cada conjunto de dados.

```python
#Discarding Data
init = 0
end = len(time) - 16
time = time[init:end]
x = x[init:end]
y = y[init:end]
vel = vel[init:end]
```



#### Dinâmica do Amarrilho

Colocando o origem do sistema na posição de equilíbrio da bola de poliestireno. Obtemos a trajetória:

![Trajectory](Media/Graphs/Amarrilho/Trajectory (Amarrilho).png)

Note agora que desta vez a trajetória não foi modelada por uma função exponencial.

Em vez disso utilizei uma equação do tipo:

$`f(t) = At^3+Bt^2+Ct+D`$

<img src="Media/Graphs/Amarrilho/x vs t (Amarrilho).png" alt="XvsT" style="zoom:70%;" /> <img src="Media/Graphs/Amarrilho/y vs t (Amarrilho).png" alt="YvsT" style="zoom:70%;" />



<img src="Media/Graphs/Amarrilho/Vx vs t (Amarrilho).png" alt="VXvsT" style="zoom:70%;" /> <img src="Media/Graphs/Amarrilho/Vy vs t (Amarrilho).png" alt="VYvsT" style="zoom:70%;" />



#### Dinâmica da Partícula de Poliestireno

Observando o gráfico da trajetória da partícula de poliestireno vemos que no final ela pode ter sido perturbada pelo amarrilho. Se isso ocorreu a recíproca também deve ser verdadeira. Se quisermos excluir os últimos dados novamente utilizar o **keyword argument**: max_rows = 150 ​

![Trajec](Media/Graphs/percevejos/Trajec.png)

Ao compararmos a modelagem polinomial com a exponencial notamos uma similaridade na sua capacidade de prever os dados experimentais. No entanto, uma solução polinomial parece mais apropriada para este caso, pois os gráficos de $`y(t)`$, $`V_y(t)`$ e $`A_y(t)`$ sugerem polinômios de graus 3, 2 e 1 respectivamente.

<img src="Media/Graphs/percevejos/XvsT.png" alt="XvsT" style="zoom:70%;" /><img src="Media/Graphs/percevejos/YvsT.png" alt="YvsT" style="zoom:70%;" />





Também podemos comparar a posição de repouso da partícula com a posição dos percevejos. 

|       |          Rosa          |          Preto          |          Amarelo          |          Partícula          |
| :---: | :--------------------: | :---------------------: | :-----------------------: | :-------------------------: |
| **x** | $x_{Rosa} = 0.189181$  | $x_{Preto} = 0.191659$  |  $x_{Amarelo} = 0.15176$  | $x_{Partícula} = 0.172265$  |
| **y** | $y_{Rosa} = 0.0462898$ | $y_{Preto} = 0.0113365$ | $y_{Amarelo} = 0.0225924$ | $y_{Partícula} = 0.0274816$ |

```python
import numpy as np
X = np.array([0.189181, 0.191659, 0.15176])
Y = np.array([0.0462898, 0.0113365, 0.0274816])
print("Mean Value\nx: %f\ty: %f"%(X.mean(), Y.mean()))

'''Mean Value
x: 0.177533	y: 0.028369'''
```

A média forneceu o resultado com boa aproximação, no entanto, neste vídeo a disposição dos percevejos está bastante simétrica.

​												$\frac{x_{Rosa}+x_{Preto}+x_{Amarelo}}{3} = 0.177533$				 $\frac{y_{Rosa}+y_{Preto}+y_{Amarelo}}{3} = 0.028369$

````python
import numpy as np
import matplotlib.pyplot as plt

#       PINK          BLACK        YELLOW
data = np.array([
    [[13.24,8.64],[16.41,11.44],[17.03,7.89]],
    [[13.36,9.02],[16.41,11.44],[16.11,8.29]],
    [[13.36,9.02],[15.81,11.89],[16.11,8.29]],
    [[13.86,9.42],[15.81,11.89],[16.11,8.29]],
    [[13.86,9.42],[15.81,11.89],[16.46,8.74]],
    [[14.26,9.64],[15.81,11.89],[16.46,8.74]],
])

pink_data = np.array([
    [13.24,8.64],
    [13.36,9.02],
    [13.36,9.02],
    [13.86,9.42],
    [13.86,9.42],
    [14.26,9.64]
])
black_data = np.array([
    [16.41,11.44],
    [16.41,11.44],
    [15.81,11.89],
    [15.81,11.89],
    [15.81,11.89],
    [15.81,11.89]
])
yellow_data = np.array([
    [17.03,7.89],
    [16.11,8.29],
    [16.11,8.29],
    [16.11,8.29],
    [16.46,8.74],
    [16.46,8.74]
])
particle_data = np.array([
    [15.01,9.64],
    [14.89,10.24],
    [14.71,10.27],
    [15.68,9.99],
    [15.31,10.14],
    [16.03,10.24]
])

#NEW ORIGIN
black_data-=pink_data
yellow_data-=pink_data
particle_data-=pink_data

for i in range(6):
    plt.scatter(black_data[i][0],black_data[i][1], marker='*', color = 'b', s=6, label = 'Black Position')
    #plt.scatter(x,y, marker='*', color = 'r', s=6, label = 'Experimental Data')
    #plt.scatter(x,y, marker='*', color = 'r', s=6, label = 'Experimental Data')
    
plt.show()
````



### Água Depositada Sobre Moedas

<img src="Media/moedas_enfileiradas-1200.jpg" alt="moedas_enfileiradas-1200" style="zoom: 67%;" />

#### Materiais Utilizados

- Seringa
- Copo com água
- Moedas diversas
- Tampa plástica
- Toalha de rosto

<img src="Media/moedas_equipamento-1200.jpg" alt="moedas_equipamento-1200" style="zoom:50%;" />

#### Procedimento Experimental Realizado

- Coloquei a moeda sobre a tampa plástica.
- Escolhi a seringa com menor subdivisão. A minha comporta $`0.2ml`$ entre as marcas menores. O erro associado às minhas medidas é portanto dessa ordem.
- Enchi a seringa com água até o último traço de marcação. No meu caso até $`5ml`$.
- Depositei a água cuidadosamente na moeda, e quando a última gota fazia a água transbordar realizava a mediação, enxugava tudo com a toalha e tornava a encher a seringa até a marcação de $`5ml`$.

#### Analise dos Resultados

Neste experimento medi o volume máximo que consegui depositar sobre uma moeda. Medi também o diâmetro da moeda e comparei com o seu [valor oficial](https://www.bcb.gov.br/dinheirobrasileiro/segunda-familia-moedas.html).

Dois tipos de moeda de 10 centavos foram utilizados, a moeda no formato antigo não está no último link, porém encontrei o valor oficial para o seu diâmetro [neste artigo](https://pt.wikipedia.org/wiki/Moeda_de_dez_centavos_do_real) da Wikipédia. 

As já não mais utilizadas moedas de 1 centavo foram três. Na wikipédia pude conferir os diâmetros medidos para [as mais recentes](https://pt.wikipedia.org/wiki/Moeda_de_um_centavo_do_real). Mas consegui encontrar também as medidas para as moedas antigas de [1 centavo](https://pt.ucoin.net/coin/brazil-1-centavo-1969-1975/?tid=23923) e de [2 centavos](https://pt.ucoin.net/coin/brazil-2-centavos-1969-1975/?tid=23937).

Também fiz a medida para uma moeda de [20 centavos de euro](https://pt.ucoin.net/coin/austria-20-euro-cent-2002-2007/?tid=15).

Todos as medidas feitas para o diâmetro coincidiram com os valores divulgados.

|      Moeda      | Volume(ml) | Diâmetro(cm) |                         Material                         |
| :-------------: | :--------: | :----------: | :------------------------------------------------------: |
|  R$0,10 (NOVA)  |    1,3     |     2,0      |                 Aço revestido de bronze                  |
|     R$0,05      |    1,8     |     2,2      |                  Aço revestido de cobre                  |
| R$0,10 (ANTIGA) |    1,6     |     2,2      |                      Aço inoxidável                      |
|     R$0,25      |    2,2     |     2,5      |                 Aço revestido de bronze                  |
|     R$0,50      |    2,0     |     2,3      |                       Cuproníquel                        |
|     R$1,00      |    2,4     |     2,7      | Aço inoxidável (núcleo) e aço revestido de bronze (anel) |
|  R$0,01 (1969)  |    0,8     |     1,7      |                      Aço inoxidável                      |
|  R$0,01 (1994)  |    1,0     |     2,0      |                      Aço inoxidável                      |
|  R$0,01 (1998)  |    1,0     |     1,7      |                  Aço revestido de cobre                  |
|  R$0,02 (1964)  |    1,2     |     1,9      |                      Aço inoxidável                      |
|  €0,20 (2002)   |    1,5     |     2,2      |                       Ouro nórdico                       |

![volume_vs_area](Media/Graphs/volume_vs_area.png)

Como a água se adere de modos distintos à materiais de composições diferentes, colori os dados com base na composição de sua camada mais externa.

As retas só foram traçadas para materiais em que eu possuía mais de uma amostra. 

No gráfico, vemos que a moeda de aço de volume $`1.0ml`$, apresenta um valor menor que o esperado, pois para moedas de mesma constituição seria esperado que o volume máximo possível de ser depositado crescesse com o diâmetro. A moeda em questão é a de R$0,01 (1994).

Quando removi essa moeda dos dados a reta verde sobrepôs a reta azul.

Vemos também que a moeda de cuproníquel está sobre a linha do cobre, que é o material que compõe a sua liga.



```python
import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize

#Volumes
volume = np.array([1.3, 1.8, 1.6, 2.2, 2.0, 2.4, 0.8, 1.0, 1.0, 1.2, 1.5])

#Diâmetro
diameter= np.array([2.0, 2.2, 2.2, 2.5, 2.3, 2.7, 1.7, 2.0, 1.7, 1.9, 2.225])

#Área
area = (np.pi/4)*diameter**2

#Material
material = np.array(['bronze', 'cobre', 'aço', 'bronze', 'cuproníquel', 'aço+bronze', 'aço', 'aço', 'cobre', 'aço', 'ouro'])

for i in ['bronze', 'cobre', 'aço', 'cuproníquel', 'aço+bronze', 'ouro']:
    
    #number of samples with this material
    indexs = np.where(material == i)[0]

    mask = []
    for j in range(len(material)):
        if j in indexs:
            mask.append(True)
        else:
            mask.append(False)

    X = area[mask]
    Y = volume[mask]
    
    plt.scatter(X,Y,marker='*', s=12, label = i)
    try:
        params = optimize.curve_fit(lambda x,A,B: A*x+ B, X, Y, [1,0])[0]
        x =  np.array([0,6, 0.1])
        plt.plot(x, params[0]*x+params[1], linewidth=1/4)
    except: pass
      
plt.xlim(0,6)
plt.ylim(0,3)        
plt.gca().set_aspect('equal', adjustable='box')
plt.xlabel('Area (cm²)')
plt.ylabel('Volume (ml)')
plt.title('Volume vs Area')
plt.legend()
plt.grid(True)
plt.show()
```

