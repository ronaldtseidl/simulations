// Colors
let colorj1 = window.getComputedStyle(document.documentElement).getPropertyValue('--colorj1');
let colorj2 = window.getComputedStyle(document.documentElement).getPropertyValue('--colorj2');
let colorm1 = window.getComputedStyle(document.documentElement).getPropertyValue('--colorm1');
let colorm2 = window.getComputedStyle(document.documentElement).getPropertyValue('--colorm2');
let colorJ = window.getComputedStyle(document.documentElement).getPropertyValue('--colorJ');
let colorM = window.getComputedStyle(document.documentElement).getPropertyValue('--colorM');
let colorCoef = window.getComputedStyle(document.documentElement).getPropertyValue('--colorCoef');
// Elements
let elej1, elej2, elem1, elem2, eleJ, eleM, eleCoef; //j1,j2=span  m1,...=td

function ordTable(divID){
    dw=0;
    dh=0;
    
    numOfTables = getNumOfTables(divID)
    for (var i = 1; i < numOfTables; i++) {
        
        let tbl = getTables(divID)[numOfTables-i];
        //tbl.width = '500px';
        //tbl.style.color = 'blue';
        let width = tbl.clientWidth+dw;
        let height = tbl.clientHeight+dh;
        dw = width;
        dh = height;
        tbl = getTables(divID)[numOfTables-i-1];
        wid2 = tbl.rows[1].cells[0].clientWidth+4; // <------------ Armengue
        hei2 = tbl.rows[0].cells[1].clientHeight+2;
        dw-=wid2;
        dh-=hei2;
        tbl.style.left= (dw).toString()+"px";
        tbl.style.top= (dh).toString()+"px";

    }
}
function getTables(divID){
    return document.getElementById(divID).children
}
function getNumOfTables(divID){
    return getTables(divID).length
}
ordTable('cg12x12')
ordTable('cg1x12')
ordTable('cg2x1')


function findTable(divID, tableNum, positionClass){
    table = document.getElementById(divID).children[tableNum].getElementsByClassName(positionClass)[0];
    // We have a m x n Tabl
    m = table.rows.length
    n = table.rows[0].cells.length;
    return [table, m, n]
}

function getElement(table, row, column){
    // Return <td> with value
    return table.getElementsByTagName("tr")[row].getElementsByTagName("td")[column]
}

function findElement(table, row, column){
    // Return String With value
    return getElement(table, row, column).innerHTML
}
function changeElementColor(element, color){
    element.style.color = color;
}
function colorElements(){
    changeElementColor(elej1, colorj1);
    changeElementColor(elej2, colorj2);
    changeElementColor(elem1, colorm1);
    changeElementColor(elem2, colorm2);
    changeElementColor(eleJ, colorJ);
    changeElementColor(eleM, colorM);
    changeElementColor(eleCoef, colorCoef);
}
function removeColors(){
    fontColor = "black"
    changeElementColor(elej1, fontColor);
    changeElementColor(elej2, fontColor);
    changeElementColor(elem1, fontColor);
    changeElementColor(elem2, fontColor);
    changeElementColor(eleJ, fontColor);
    changeElementColor(eleM, fontColor);
    changeElementColor(eleCoef, fontColor);
}




document.getElementById("findBut").onclick = function(){

    if(elej1!=null){ // elej1,elej2,... só recebem um valor se esse script já foi rodado.
        removeColors();
    }

    j1 = document.getElementById("inputj1").value;
    m1 = document.getElementById("inputm1").value;
    j2 = document.getElementById("inputj2").value;
    m2 = document.getElementById("inputm2").value;
    J = document.getElementById("inputJ").value;
    M = document.getElementById("inputM").value;

    j1Txt = j1;
    j2Txt = j2;
    if (j1.length==3){j1Txt=j1[0]+j1[2]}   // Retirando a barra /
    if (j2.length==3){j2Txt=j2[0]+j2[2]}

    divID = "cg"+j1Txt+"x"+j2Txt; // "cg1x12"
    numOfTables = getNumOfTables(divID);


    elej1 = document.getElementById(divID).children[numOfTables-1].getElementsByClassName("numj1")[0]
    elej2 = document.getElementById(divID).children[numOfTables-1].getElementsByClassName("numj2")[0]
    
    // Finding Row of cgCoef with m1,m2
    for (i=1; i<=numOfTables; i++){


        auxTableInfo = findTable(divID, i-1, "pos21")
        table = auxTableInfo[0]
        numberOfRows = auxTableInfo[1]
        numberOfColumns = auxTableInfo[2]
        for (j=0; j<numberOfRows; j++){
            Tj0 = findElement(table, j, 0)
            Tj1 = findElement(table, j, 1)
            //if(Tj0[0]=='+'){sign0='+'}
            //else{sign0=''}  
            //if(Tj1[0]=='+'){sign1='+'}
            //else{sign1=''} 
            if(parseFloat(m1) ==parseFloat(Tj0) && parseFloat(m2) ==parseFloat(Tj1)){
                var cgSubTable=i; //i>=1
                cgRow=j; //j>=0
                elem1 = getElement(table, cgRow, 0)
                elem2 = getElement(table, cgRow, 1)

                break;
            }                  
        }                                        
    }

    // Finding Column of cgCoef, with J, M
    for (i=1; i<=numOfTables; i++){
       
        auxTableInfo = findTable(divID, i-1, "pos12")
        table = auxTableInfo[0]
        numberOfRows = auxTableInfo[1]
        numberOfColumns = auxTableInfo[2]
        for (j=0; j<numberOfColumns; j++){
            Tj0 = findElement(table, 0, j)
            Tj1 = findElement(table, 1, j)
            //if(Tj0[0]=='+'){sign0='+'}
            //else{sign0=''}  
            //if(Tj1[0]=='+'){sign1='+'}
            //else{sign1=''} 
            if(parseFloat(J) ==parseFloat(Tj0) && parseFloat(M) ==parseFloat(Tj1)){
                var cgSubTable2=i; //i>=1
                cgColumn=j; //j>=0
                eleJ = getElement(table, 0, cgColumn)
                eleM = getElement(table, 1, cgColumn)
                break;
            }                  
        }                                        
    }
    if(cgSubTable==cgSubTable2){
       table = findTable(divID, cgSubTable-1, "pos22")[0]
        
        cgCoef = findElement(table, cgRow, cgColumn)
        eleCoef = getElement(table, cgRow, cgColumn);
        if(cgCoef[0]=="-"){sign="-"; cgCoef=cgCoef.slice(1)}
        else{sign=""}
        // Verificando para saber se escrevo ou não com a raiz quadrada.
        if (cgCoef=="1"||cgCoef=="0"){
            document.getElementById("myCoef").innerHTML = sign+cgCoef;
        } else{
            document.getElementById("myCoef").innerHTML = sign+"sqrt("+cgCoef+")";
        }
        
        colorElements();
        
    }
    else{
        document.getElementById("myCoef").innerHTML = "0"
         
        removeColors();
    }
};