//Canvas
const canvasWidth = Math.min(screen.width, screen.height-150);
const canvasHeight = Math.min(screen.width, screen.height-150);
let graph;
//Constants
const gravity = 10;
const dt = 1/120;
//Options
const integrationMethod = 2; // 1=Euler(Faster)  2=Verlet(Better)

function setup() {
  //Creating Canvas
  let canvas = createCanvas(canvasWidth, canvasHeight);
  canvas.position((screen.width-canvasWidth)/2,0);
  graph = createGraphics(canvasWidth, canvasHeight);
  //Pendulum object
  p = new Pendulum();
}

function draw() {
  //Background
  background(240);

  //Trajectory
  image(graph,0,0);

  let txtSz = canvasWidth/40;
  textSize(txtSz);
  //Massas
  text("m₁ =  " + p.mass.toFixed(1),canvasWidth/2 - 3*txtSz, canvasHeight-txtSz)
  text("m₂ =  " + p.mass2.toFixed(1),canvasWidth/2 + 3*txtSz, canvasHeight-txtSz)
  //theta2
  text("θ₂ = " + p.angle2.toFixed(2),txtSz, canvasHeight-txtSz);
  //Energy
  text("E = " + p.E.toFixed(2),txtSz, canvasHeight-3*txtSz);

  //Pendulum
  p.draw();
  p.update();

}

//Creating Pendulum  Class
class Pendulum{
    constructor(){
      // Pendulum 1
      this.origin = new p5.Vector(0,0);//new p5.Vector(canvasWidth/2,canvasHeight/10);
      this.mass = map(Math.random(),0,1, 1, 10);
      this.L = Math.random();
      this.LPixels = map(this.L,0,1,canvasWidth/10,canvasWidth/4);
      this.radius1 = (1/50)*canvasHeight*(3*this.mass/(4*PI))**(1/3);
      this.angle = map(Math.random(),0,1,PI/4,PI/2);
      this.prevθ = this.angle;
      this.aVelocity = 0;
      this.aAceleration = 0;
      this.position = this.getPosition();
      
      // Pendulum 2
      this.mass2 = map(Math.random(),0,1, 1, 10);
      this.L2 = Math.random();
      this.LPixels2 = map(this.L2,0,1,canvasWidth/10,canvasWidth/4);
      this.radius2 = (1/50)*canvasHeight*(3*this.mass2/(4*PI))**(1/3);
      this.angle2 = map(Math.random(),0,1,-PI/2,PI/2);
      this.prevθ2 = this.angle2;
      this.aVelocity2 = 0;
      this.aAceleration2 = 0;
      this.position2 = this.getPosition2();

      // System
      this.M = this.mass+this.mass2;
      this.dtheta = this.angle-this.angle2;
      this.Lfrac = this.L/this.L2;
      this.K = this.getKinetic();
      this.U = this.getPotential();
      this.E = this.K + this.U;
      this.accConstrain = 0.5;
      this.oldPosition = this.position.copy();
      this.oldPosition2 =  this.position2.copy();
      
    }
    
    draw() {
      const O = new p5.Vector(canvasWidth/2,canvasHeight/10);
      let r1 = this.position.copy().normalize().mult(this.LPixels).add(O);
      let r2 = this.position2.copy().normalize().mult(this.LPixels2).add(r1);
      let OP = this.oldPosition.copy().normalize().mult(this.LPixels).add(O);
      let OP2 = this.oldPosition2.copy().normalize().mult(this.LPixels2).add(OP);

      //Rod
      fill(0, 0, 0);
      line(O.x, O.y, r1.x, r1.y);
      //Rod2
      fill(0, 0, 0);
      line(r1.x, r1.y, r2.x, r2.y);
      //Ball
      fill(42);
      ellipse(r1.x, r1.y, this.radius1, this.radius1);
      //Ball2
      fill(42);
      ellipse(r2.x, r2.y, this.radius2, this.radius2);

      //Trajectory Graph
      graph.stroke(184);
      graph.line(OP2.x, OP2.y, r2.x, r2.y);
    }
    update(){
      this.oldPosition = this.position.copy();
      this.oldPosition2 = this.position2.copy();

      //Acceleration
      let num = (this.M)*gravity*sin(this.angle)+this.mass2*(this.L2*sin(this.dtheta)*this.aVelocity2**2 + cos(this.dtheta)*(this.L*sin(this.dtheta)*this.aVelocity**2-gravity*sin(this.angle2)))
      let denom = this.L*(this.mass+this.mass2*(sin(this.dtheta))**2)
      this.aAceleration =-num/denom;
      console.log(denom)

      num = -gravity*sin(this.angle2) - this.L*(-sin(this.dtheta)*this.aVelocity**2 + cos(this.dtheta)*this.aAceleration);
      this.aAceleration2 = num/this.L2;

      
      if (integrationMethod==1){
        this.aVelocity += this.aAceleration*dt;
        this.aVelocity2 += this.aAceleration2*dt;

        this.angle += this.aVelocity*dt;
        this.angle2 += this.aVelocity2*dt;

        this.position = this.getPosition();
        this.position2 = this.getPosition2();
      }
      else if(integrationMethod==2){
        // https://www.algorithm-archive.org/contents/verlet_integration/verlet_integration.html
        //Position (Verlet Integration)
        let auxAng1 = this.angle;
        this.angle = 2*this.angle - this.prevθ + this.aAceleration*dt**2
        let auxAng2 = this.angle2;
        this.angle2 = 2*this.angle2 - this.prevθ2 + this.aAceleration2*dt**2
        //New Positions
        this.position = this.getPosition();
        this.position2 = this.getPosition2();
        //α
        this.aVelocity = (this.angle-this.prevθ)/(2*dt)
        this.aVelocity2 = (this.angle2-this.prevθ2)/(2*dt)
        //prevθ
        this.prevθ = auxAng1;
        this.prevθ2 = auxAng2;
      }

      //Update dtheta
      this.dtheta = this.angle-this.angle2;

      //Update Energy
      this.K = this.getKinetic()
      this.U = this.getPotential()
      this.E = this.K + this.U

    }   
    getPosition() {
      //Posição da massa 1 no referencial do nó.
      let position = new p5.Vector(sin(this.angle), cos(this.angle));
      position.mult(this.L);
      //position.add(this.origin);
      return position;
    }
    getPosition2() {
      //Posição da massa 2 no referencial da massa 1
      let position = new p5.Vector(sin(this.angle2), cos(this.angle2));
      position.mult(this.L2);
      //position.add(this.position);
      return position;
    }
    getPotential(){
      return -(this.mass+this.mass2)*gravity*this.L*cos(this.angle) - this.mass2*gravity*this.L2*cos(this.angle2)
    }
    getKinetic(){
      return this.mass*(this.L*this.L)*(this.aVelocity*this.aVelocity)/2 + this.mass2*((this.L*this.L)*(this.aVelocity*this.aVelocity)+(this.L2*this.L2)*(this.aVelocity2*this.aVelocity2) + 2*this.L*this.L2*cos(this.dtheta)*this.aVelocity*this.aVelocity2)/2
    }
  };


/*
Para poder utilizar os métodos e atributos do p5 dentro da minha classe Pendulum, tive de definir o objeto p dentro de setup.
*/

