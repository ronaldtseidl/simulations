///////GLOBAL VARIABLES////////////////////////////////////////////////////////////
let canvasSide = Math.min(screen.width, screen.height-130);
let partitionNumber = 160; //How many blocks we can put horizontaly side by side.
let blockSide = Math.floor(canvasSide/partitionNumber + 1);//Without 1 more pixel the blockSide will be small and will not fill the canvas
let stepPerSecond = 80;
let falledBlocks  //Graphics with all falled blocks
let p = 0.5;
let fallVelocity = blockSide

class Block{
    constructor(x,y){
        this.x = x
        this.y = 0
        this.fallVelocity = fallVelocity
        this.column = Math.floor(x/blockSide)
    }
}
class Squares{
    constructor(){
        this.N = 0                                              //Number of blocks falling
        this.blocks = []                                        //List containing all falling blocks 
        this.ground = Array(partitionNumber+0).fill(canvasSide)   //List of y positions of the each column highest blocks
        this.pos = partitionNumber/2                            //The next block column index
        this.ground[this.pos]++

    }
    step(){
        let rand = random()
        if (rand < p){
            this.pos += 1
        } else if (rand > p){
            this.pos -= 1
        } else{this.step()}
        //this.pos += (-1)**(round(random()));
        this.N++;
        this.blocks.push(new Block(this.pos*blockSide));
    }
    draw(){
        let deleteBlockList = []
        falledBlocks.fill(64, 61, 61)
        image(falledBlocks, 0, 0)
        for (let i = 0; i<this.N;i++){
            fill(250,0,0)
            rect(this.blocks[i].x,this.blocks[i].y, blockSide, blockSide);
            if (this.blocks[i].y < this.ground[this.blocks[i].column] - blockSide){
                this.blocks[i].y+=this.blocks[i].fallVelocity;}
            else{
                this.ground[this.blocks[i].column]-=blockSide;
                this.blocks[i].y = this.ground[this.blocks[i].column]
                this.blocks[i].fallVelocity=0

               falledBlocks.rect(this.blocks[i].x,this.blocks[i].y, blockSide, blockSide);
               deleteBlockList.push(i)
            }
        }
       deleteBlockList.forEach(index => {this.blocks.splice(index, 1)})
       this.N-=deleteBlockList.length
    }
}
const squares = new Squares()

function setup(){
    let canvas = createCanvas(canvasSide,canvasSide);
    //canvas.position(400,50)
    falledBlocks = createGraphics(canvasSide, canvasSide);

}

function draw() {
    background(242, 242, 242);
    fill(64, 61, 61);
    squares.draw()
    if (frameCount % (60/stepPerSecond) == 0){
        squares.step()
    }
     
};