let canvasSide = Math.min(screen.width, screen.height-150);
let canvasIsClicked = false;
let proportion = canvasSide/714
let butReInit;
let canvas;
let backImage;
//////////////////////////////////////SETUP///////////////////////////////////////////////////////////////////////////////////////////
function setup() {
  //Creating Canvas
  canvas = createCanvas(canvasSide, canvasSide);
  //canvas.parent("canvasID");
  canvas.mouseClicked(function(){canvasClicked = true});
  backImage = createGraphics(canvasSide, canvasSide);
  for (i=0; i<canvasSide; i++){
    for (j=0; j<canvasSide; j++){
        if (Math.random(1)>0.998){
            let lightStar = map(Math.random(),0,1,0,255);
            backImage.stroke(lightStar, lightStar, lightStar, 99);
            backImage.ellipse(i,j,1,1);
        }
    }
  }

  //Creating Drag Button
  butReInit = createButton('Reiniciar');
  butReInit.position(canvas.x+canvasSide/2-butReInit.width/2,canvas.y + canvasSide-butReInit.height, 65);
  butReInit.mouseReleased(butReInitPressed);
  butReInit.style('color:green')
}

/////////////////////////////////////Buttons//////////////////////////////////////////////////////////////////////////////////////////////
const butReInitPressed = function(){
  particles = []
  particles.pop()
}

const changeDrag = function(){
  if (drag){
    document.getElementById("Drag").style.color="red"
  } else{document.getElementById("Drag").style.color="green"}
  drag = !drag
  particles.pop()
}

///////////////////////////////////PARTICLE CLASS/////////////////////////////////////////////////////////////
//Creating Particle  Class
var Particle = function(position){
    this.mass = 1;
    this.position = position;
    this.oldPos = this.position.copy();
    this.radius = canvasSide/70;
    this.vel = new p5.Vector(0, 0);
    this.acc = 0;
    this.isAttractor = false;
    this.color = [63, 55, 201]

    this.applyForce = function(){
      let G = 200
      let totalForce = new p5.Vector(0,0);
      
      let force = att.position.copy().sub(this.position)
      let dist = force.mag()
      force.mult(G*this.mass*att.mass/dist**3);
      totalForce.add(force)
      return totalForce
    }

    this.draw = function() {
      //Particle
      fill(this.color[0], this.color[1], this.color[2]);
      ellipse(this.position.x, this.position.y, this.radius, this.radius);

      //New Particle Initial Velocity
      if (mouseIsPressed){
        stroke(200,60,50)
        let lastParticle = particles[particles.length-1];
        line(lastParticle.position.x, lastParticle.position.y, mouseX, mouseY)
      }
   
    }
    
    this.update = function(){
      if (!mouseIsPressed&&!this.isAttractor){
        this.acc = this.applyForce(this.position);
        
        this.vel.add(this.acc)
        this.position.add(this.vel)}
    }   
    
};

/////////////////////////////////////MOUSE//////////////////////////////////////////////////
function mousePressed(){
  particles.push(new Particle(new p5.Vector(mouseX,mouseY)));
}
function mouseReleased(){
  let reductionFactor = 10;
  let lastParticle = particles[particles.length-1]
  lastParticle.vel = new p5.Vector(mouseX - lastParticle.position.x, mouseY - lastParticle.position.y)
  lastParticle.vel.div(reductionFactor)
}

////////////////////////////////CREATING PARTICLES//////////////////////////////////////////////

//Particle object
var particles = []

//Attractors
let att = new Particle(new p5.Vector(canvasSide/2,canvasSide/2));
att.isAttractor = true
att.radius = 40*proportion
att.mass = 40;
att.color = [244, 140, 6]

//////////////////////////////Draw Stars/////////////////////////////////////////////////////////
const drawStars = function(){
  
};

drawStars()

//////////////////////////////////DRAW/////////////////////////////////////////////////////////
function draw() {
  noStroke();
  //Background
  fill(3, 7, 30,50)
  rect(0, 0, canvasSide, canvasSide); 

  image(backImage,0,0)

  //Particle
  for (i=0; i<particles.length; i++){
    particles[i].draw();
    particles[i].update();}

  //Attractors
  att.draw();
  att.update();

  //Ground
  fill(100,0,142);
  rect(0, canvasSide - butReInit.height, canvasSide, butReInit.height);

}