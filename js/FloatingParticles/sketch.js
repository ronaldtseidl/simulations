let drag = true;
let showField = true;
let fieldDensity = 40  //4 is beautiful
let forceType = 1;  //0- 1/r²  1- exp(-kt)
let invertOrientation = false; //Attractive forces become repulsive and vice versa
let canvasSide = Math.min(screen.width, screen.height-150);
let canvasIsClicked = false;
let proportion = canvasSide/714
fieldDensity *= proportion
let butReInit;
let canvas;
let fieldCanvas;
let needChangeField = true; //Tell us if we need to recalculate the field

//////////////////////////////////////SETUP///////////////////////////////////////////////////////////////////////////////////////////
function setup() {
  //Creating Canvas
  canvas = createCanvas(canvasSide, canvasSide);
  canvas.parent("canvasID")
  canvas.mouseClicked(function(){canvasClicked = true})
  //canvas.mouseReleased(function(){canvasClicked = false})
  //canvas.position((screen.width-canvasSide)/2,0);
  //canvas.center();
  fieldCanvas = createGraphics(canvasSide, canvasSide);

  //Creating Drag Button
  butReInit = createButton('Reiniciar');
  console.log(canvas.x)
  butReInit.position(canvas.x+canvasSide/2-butReInit.width/2,canvas.y + canvasSide-butReInit.height, 65);
  butReInit.mouseReleased(butReInitPressed);
  butReInit.style('color:green')
}

/////////////////////////////////////Buttons//////////////////////////////////////////////////////////////////////////////////////////////
const butReInitPressed = function(){
  particles = []
  particles.pop()
}

const changeDrag = function(){
  if (drag){
    document.getElementById("Drag").style.color="red"
  } else{document.getElementById("Drag").style.color="green"}
  drag = !drag
  particles.pop()
}
const changeField = function(){
  if (showField){
    document.getElementById("Field").style.color="red"
  } else{document.getElementById("Field").style.color="green"}
  showField = !showField
  particles.pop()
}

const changeFieldDensity = function(value){
  fieldDensity = 4*value*proportion
  needChangeField = true;
}

///////////////////////////////////PARTICLE CLASS/////////////////////////////////////////////////////////////
//Creating Particle  Class
var Particle = function(position){
    this.mass = 1;
    this.position = position
    this.radius = canvasSide/70;
    this.vel = new p5.Vector(0, 0);
    this.acc = 0;
    this.isAttractor = false;
    this.color = [Math.round(255*Math.random()),Math.round(255*Math.random()),Math.round(255*Math.random())]

    this.applyForce = function(){
      let G = 90
      let A = 15
      let totalForce = new p5.Vector(0,0);
      for (j=0; j<att.length; j++){
        let force = att[j].position.copy().sub(this.position)
        let dist = force.mag()
        if (forceType==0) {force.mult(-G*this.mass*att[j].mass/dist**3);}
        if (forceType==1) {force.mult((dist-15)/500000);}
        if (invertOrientation){force.mult(-1)}
        totalForce.add(force)}
      return totalForce
    }

    this.draw = function() {
      //Particle
      fill(this.color[0], this.color[1], this.color[2]);
      ellipse(this.position.x, this.position.y, this.radius, this.radius);

      //New Particle Initial Velocity
      if (mouseIsPressed){
        stroke(200,60,50)
        let lastParticle = particles[particles.length-1];
        line(lastParticle.position.x, lastParticle.position.y, mouseX, mouseY)
      }
   
    }
    
    this.update = function(){
      if (!mouseIsPressed&&!this.isAttractor){
        this.acc = this.applyForce(this.position);
        this.vel.add(this.acc)
        if (drag){this.vel.mult(90/100)}
        this.position.add(this.vel)}
        if (this.vel.length==0){print(this.position)}
    }   
    
};

/////////////////////////////////////CREATE FIELD////////////////////////////////////////////////////
const createField = function(){
  fieldCanvas.clear()
  for (y=0; y<canvasSide; y+=fieldDensity){
    for (x=0; x<canvasSide; x+=fieldDensity){
      field.position.x = x
      field.position.y = y
      field.position = field.applyForce()
      let len = field.position.mag()
      field.position.setMag(8*proportion)
      
      fieldCanvas.strokeWeight(proportion)
      fieldCanvas.stroke(70, 50, 20)
      fieldCanvas.line(x ,y, x+field.position.x, y+field.position.y)
    }
  }
}

/////////////////////////////////////MOUSE//////////////////////////////////////////////////
function mousePressed(){
  //console.log(event.target.id)
  particles.push(new Particle(new p5.Vector(mouseX,mouseY)));
  print([mouseX,mouseY])
}
function mouseReleased(){
  let reductionFactor = 10;
  let lastParticle = particles[particles.length-1]
  lastParticle.vel = new p5.Vector(mouseX - lastParticle.position.x, mouseY - lastParticle.position.y)
  lastParticle.vel.div(reductionFactor)
}

////////////////////////////////CREATING PARTICLES AND FIELD//////////////////////////////////

//Particle object
var particles = []

//Attractors
let att = []
for (i=0; i<3; i++){
  let n = 15
  let pos = [[0,37*n*proportion],[36*n*proportion,40*n*proportion],[25*n*proportion,0]] //[[200,300],[250,500],[400,200]] 
  console.log(i, pos[i])
  att[i] = new Particle(new p5.Vector(pos[i][0],pos[i][1]));
  att[i].isAttractor = true
  att[i].radius = 40*proportion
  att[i].mass = 40;}

//Field
let field = new Particle(new p5.Vector(0,0))


//////////////////////////////////DRAW/////////////////////////////////////////////////////////
function draw() {
  //Background
  fill(20,60,190,50)
  rect(0, 0, canvasSide, canvasSide);

  //Field
  if (showField){
    console.log(needChangeField)
    image(fieldCanvas,0, 0)
    if (needChangeField){
      fieldList = createField();
      needChangeField = false}
    }
  //Particle
  for (i=0; i<particles.length; i++){
    particles[i].draw();
    particles[i].update();}

  //Attractors
  for (i=0; i<att.length; i++){
    att[i].draw();
    att[i].update();}

  //Ground
  fill(100,0,142);
  rect(0, canvasSide - butReInit.height, canvasSide, butReInit.height);

}