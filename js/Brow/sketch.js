let canvasWidth = Math.min(window.innerWidth, window.innerHeight-20);
let canvasHeight = canvasWidth;
let butTraj;
let showTraj = false;
let suavFactor = 5;
let trajCanvas;


function setup() {
  //Creating Canvas
  let canvas = createCanvas(canvasWidth, canvasHeight);
  canvas.parent("canvasConteiner");
  canvas.position((window.innerWidth-canvasWidth)/2,0);
  trajCanvas = createGraphics(canvasWidth, canvasHeight);
  //Creating Drag Button
  butTraj = createButton('Trajetoria');
  butTraj.position(canvas.x+canvasWidth/2-butTraj.width/2,canvas.y + canvasHeight-butTraj.height, 65);
  butTraj.mousePressed(butTrajPressed);
  butTraj.style('color:red')
  butTraj.parent("canvasConteiner");
}
let butTrajPressed = function(){
  showTraj = !showTraj;
  if (showTraj){butTraj.style('color:green')}
  else{butTraj.style('color:red')}
  trajCanvas.clear()
}
///////////////////Creating Particle  Class///////////////////////////////////////////////////////////////////
var Particle = function(){
    this.position = this.randomPosition();
    this.radius = 10;
    this.Velocity = new p5.Vector(0, 0);
    this.maxVel = 40;
    this.stepFraction=1;

    this.draw = function() {
      //Particle
      fill(247, 220, 113);
      ellipse(this.position.x, this.position.y, this.radius, this.radius);
    }
    this.randomVelocity = function(){
      let vx = this.maxVel*(2*Math.random()-1)
      let vy = this.maxVel*(2*Math.random()-1)
      return new p5.Vector(vx, vy)
    }
    this.update = function(){
      if (this.stepFraction > suavFactor){
        this.Velocity = this.randomVelocity()
        this.stepFraction=1}
      else{this.position.add(this.Velocity.div(suavFactor));
           this.stepFraction++;}
    }   
    
};
Particle.prototype.randomPosition = function() {

  return new p5.Vector(map(Math.random(), 0, 1 , 0 , canvasWidth), map(Math.random(), 0, 1 , 0 , canvasHeight));
};

const map = function(n, start1, stop1, start2, stop2){
  let newval = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2;
  return newval
}

//Particle object
var p = new Particle();


///////////////////////////DRAW//////////////////////////////////////////////////////////
function draw() {
  //Background
  fill(0, 184, 230);
  rect(0, 0, canvasWidth, canvasHeight);

  
  //Particle and Trajectory
  let oldPosition = p.position.copy()
  console.log(p.position)
  p.update();
  if (showTraj){
    trajCanvas.stroke(0)
    //trajCanvas.strokeWeight(3)
    trajCanvas.line(oldPosition.x, oldPosition.y, p.position.x, p.position.y)
    image(trajCanvas, 0, 0)
  }
  p.draw();
  
  //Ground
  fill(100,153,142);
  rect(0, canvasHeight - butTraj.height, canvasWidth, butTraj.height);

}
